<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'laravelapp' );

/** MySQL database username */
define( 'DB_USER', 'alfimysql' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{?hlJI)H~Vu?i62MU4V9!7t]1I+Ui;|3}o``Z.4_`u=j!Jp_I+lk)JH=/U9TVs4w' );
define( 'SECURE_AUTH_KEY',  'g?3Y-.+y[?AB(}8>,QHn}b$E35EzOLDK()g_M5;xh5ljc^NlWS1jZ+V5GL<2#Y-`' );
define( 'LOGGED_IN_KEY',    'cVP^:M$t-6unrZZ~34zXN;S4i]Ba?y]l?DN]k>gBvU%j8(S!Q:<f#*G=$ZF3S]i[' );
define( 'NONCE_KEY',        'bj:c]6*D^@s.v~b@]!b6u6Q{vm=`/|6!Gyl8F.EF0:KO,Qmng}bAV~OQx,u70Qy;' );
define( 'AUTH_SALT',        'O?naFb:C>|/Zt3Qa,!Q<?yp_FdDs0bK(+4]UR=P!hPJ#5TZg-op;[Yo1vtJ7HyZD' );
define( 'SECURE_AUTH_SALT', 'ka7]LNm:<]lc{FKVAa=Y9d/uHBt^OQS38D|*2<x,YoN-~D}[nf1v#3{m/1[yIi=/' );
define( 'LOGGED_IN_SALT',   'h6)~>42>{$^^.o3q6~.D{`P6VD XYA@W-lL$m%XmBwXMdV^.08ueC##w3Jy{&%fp' );
define( 'NONCE_SALT',       'NBV198f o5hwtBpI5/ W:IBf~LKr7.)F[S|1i #zyg,Rpd+2=gid5t5Oo51L?xm(' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
define( 'UPLOADS', 'wp-content/uploads' );
require_once( ABSPATH . 'wp-settings.php' );
